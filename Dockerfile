FROM nginx:alpine

RUN apk add --no-cache --virtual .cert-deps openssl && \
    openssl dhparam -out /etc/nginx/dhparam.pem 2048 && \
    apk del .cert-deps

EXPOSE 443

CMD ["nginx", "-g", "daemon off;"]
